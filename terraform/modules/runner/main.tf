resource "helm_release" "gitlab_runner" {
  name       = "gitlab-runner"
  repository = "https://charts.gitlab.io"
  chart      = "gitlab-runner"

  set {
    name  = "gitlabUrl"
    value = "https://gitlab.com/"
  }

  set {
    name  = "runnerRegistrationToken"
    value = "glrt-Gy5rzsQCbyVprz8X-YzZ"
  }

  #https://gitlab.com/gitlab-org/charts/gitlab-runner/blob/main/values.yaml
  #disable this because of this bug: https://gitlab.com/gitlab-org/charts/gitlab-runner/-/issues/353
  # set {
  #   name  = "rbac.create"
  #   value = "true"
  # }

  set {
    name  = "runners.runUntagged"
    value = "false"
  }
  set {
    name  = "runners.tags"
    value = "dev"
  }
}